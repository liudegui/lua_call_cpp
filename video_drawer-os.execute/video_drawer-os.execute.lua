
local function read_file(fileName)
    local f = assert(io.open(fileName,'r'))
    local content = f:read('*all')
    f:close()
    return content
end

local function test_draw()
    local content = read_file("detect_result.txt")
    print(#content)
    local cmd = "./bin/video_bbox_drawer" .. " " .. 
                "face_1280_720.h264" .. " " .. 
                content .. " " .. 
                #content .. " ".. 
                "face_1280_720_output.h264"
    os.execute(cmd) 
    --注:如果是openresty的代码调用，应该是使用shell.run调用
    --如：local ok, stdout, stderr, reason, status = shell.run(cmd, nil, timeout)
end

test_draw()
